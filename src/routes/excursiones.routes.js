import { Router } from "express";
import {
  createExcursiones,
  deleteExcursiones,
  editExcursiones,
  renderExcursiones,
  updateExcursiones,
} from "../controllers/excursionesController.js";
const router = Router();

router.get("/", renderExcursiones);
router.post("/add", createExcursiones);
router.get("/update/:id", editExcursiones);
router.post("/update/:id", updateExcursiones);
router.get("/delete/:id", deleteExcursiones);

export default router;